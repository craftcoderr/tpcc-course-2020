cmake_minimum_required(VERSION 3.9)

add_subdirectory(executors)
add_subdirectory(futures)
